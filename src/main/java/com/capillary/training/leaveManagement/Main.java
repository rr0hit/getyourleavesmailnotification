package com.capillary.training.leaveManagement;

public class Main 
{
    public static void main( String[] args )
    {
        Thread reminder = new Thread(new ReminderMailer());
        reminder.start();
        Thread notifier = new Thread(new NotificationMailer());
        notifier.start();
        try {
			reminder.join();
			notifier.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
}
