package com.capillary.training.leaveManagement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class NotificationMailer 
	extends Mailer 
	implements Runnable {
	
	private static final String query = "SELECT leaves.leaveID, leaves.status, " +
	"leaves.fromDate, leaves.toDate, " +
	"leaves.reason, leaves.remarks, leaves.type, " +
	"emp.name as applicantName, " +
	"man.name as managerName, " +
	"emp.email as applicantEmail, " +
	"man.email as managerEmail, " +
	"leaves.manID as managerID " +
	"FROM leaves " +
	"INNER JOIN employees as emp " +
	"ON emp.empID = leaves.empID " +
	"INNER JOIN employees as man " +
	"ON man.empID = leaves.manID " +
	"WHERE leaves.isNotified = 0 " +
	"AND leaves.isActive = 1 " +
	"AND leaves.fromDate > now();";

	public void run() {
		while (true) {
			try {
				Class.forName("com.mysql.jdbc.Driver");
				Connection con = DriverManager.getConnection(
						mysqlServer, 
						mysqlUser, 
						mysqlPassword);
				Statement stmt = con.createStatement();
				stmt.executeUpdate("use GetYourLeave;");
				ResultSet res = stmt.executeQuery(query);
				ResultSet conf = con.createStatement().executeQuery(
						"SELECT newLeaveTemplate, " + 
								"updateLeaveTemplate " +
								"FROM config;");
				conf.next();
				String newAppMail = conf.getString("newLeaveTemplate");
				String updateMail = conf.getString("updateLeaveTemplate");

				while (res.next()) {
					String subject = new String();
					String mailto = new String();
					String message = new String();
					if (res.getString("status").equals("pending")) {
						subject = "New leave application";
						message = applyTemplate(res, newAppMail);
						mailto = res.getString("managerEmail");
					} else {
						subject = "Update on your leave application";
						message = applyTemplate(res, updateMail);
						mailto = res.getString("applicantEmail");
					}

					send(subject, message, mailto);
					
					con.createStatement().executeUpdate(
							"UPDATE leaves " + "SET isNotified = 1 "
									+ "WHERE leaveID = "
									+ res.getString("leaveID"));
				}

				res.close();
				stmt.close();
				con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				Thread.sleep(60000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
