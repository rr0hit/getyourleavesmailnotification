package com.capillary.training.leaveManagement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class ReminderMailer 
	extends Mailer 
	implements Runnable {
		private static final String query = 
				"SELECT leaves.leaveID, leaves.status, " +
				"leaves.fromDate, leaves.toDate, " +
				"leaves.reason, leaves.remarks, leaves.type, " +
				"emp.name as applicantName, " +
				"man.name as managerName, " +
				"emp.email as applicantEmail, " +
				"man.email as managerEmail, " +
				"leaves.manID as managerID, " +
				"rem.template, rem.ccList " +
				"FROM leaves " +
				"INNER JOIN reminders as rem  " +
				"ON leaves.fromDate = DATE_ADD(CURDATE(), " +
				"INTERVAL rem.daysBefore DAY) " +
				"INNER JOIN employees as emp " +
				"ON emp.empID = leaves.empID " +
				"INNER JOIN employees as man " +
				"ON man.empID = leaves.manID " +
				"WHERE leaves.isActive = 1;"; 
		
	public void run() {
		while (true) {
			try {
				Class.forName("com.mysql.jdbc.Driver");
				Connection con = DriverManager.getConnection(
							mysqlServer,
							mysqlUser,
							mysqlPassword);
				Statement stmt = con.createStatement();
				stmt.executeUpdate("use GetYourLeave;");
				ResultSet res = stmt.executeQuery(query);
				while (res.next()) {
					String subject = new String();
					String mailto = new String();
					String message = new String();

					subject = "Leave reminder";
					message = applyTemplate(res, res.getString("template"));
					mailto = applyTemplate(res, res.getString("ccList"));

					send(subject, message, mailto);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				Thread.sleep(86400000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
