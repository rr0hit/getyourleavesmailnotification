package com.capillary.training.leaveManagement;

import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONObject;

public class Mailer {
	
	private static final String confFile = "/home/capillary/leave_management_java.json";
	private String emailPassword;
	private String emailUser;
	protected String mysqlPassword;
	protected String mysqlUser;
	protected String mysqlServer;
	private Session session;
	
	public Mailer() {
		
		JSONParser parser = new JSONParser();
		try {
			JSONObject jsonObj = (JSONObject) parser.parse(new FileReader(confFile));
			mysqlServer = (String) jsonObj.get("mysqlServer");
			mysqlUser = (String) jsonObj.get("mysqlUser");
			mysqlPassword = (String) jsonObj.get("mysqlPassword");
			emailUser = (String) jsonObj.get("emailUser");
			emailPassword = (String) jsonObj.get("emailPassword");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
 
		session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(emailUser, emailPassword);
			}
		  });
		
		session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(emailUser, emailPassword);
				}
			});
	}
	
	protected void send(String subject, String messageToSend, String recipients) {
		try {
			 
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("rohitforktj@gmail.com"));
			message.setRecipients(Message.RecipientType.CC,
				InternetAddress.parse(recipients));
			message.setSubject(subject);
			message.setText(messageToSend);
			Transport.send(message);
			
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
	protected String applyTemplate(ResultSet res, String template) {
		String message = new String();
		
		try {
			message = template.replace(
						"[MANAGER]", 
						res.getString("managerName")
						);
			
			message = message.replace(
						"[APPLICANT]", 
						res.getString("applicantName")
						);
			
			message = message.replace(
						"[STATUS]", 
						res.getString("status")
						);			
			
			message = message.replace(
						"[FROM]", 
						res.getString("fromDate")
						);
			
			message = message.replace(
						"[TO]", 
						res.getString("toDate")
						);
			
			message = message.replace(
					"[REASON]", 
					res.getString("reason")
					);
			
			message = message.replace(
					"[REMARKS]", 
					res.getString("remarks")
					);
			
			message = message.replace(
					"[TYPE]", 
					res.getString("type")
					);
			
			message = message.replace(
					"[MANAGEREMAIL]", 
					res.getString("managerEmail")
					);
			
			message = message.replace(
					"[APPLICANTEMAIL]", 
					res.getString("applicantEmail")
					);
			
			if(message.contains("[TEAM]")) {
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection con = DriverManager.getConnection(
							mysqlServer,
							mysqlUser, 
							mysqlPassword);
					Statement stmt = con.createStatement();
					stmt.executeUpdate("use GetYourLeave;");
					ResultSet team = stmt.executeQuery(
							"SELECT email " +
							"FROM employees " +
							"WHERE manID = " + res.getString("managerID"));
					
					String teamEmail = new String();
					while(team.next()) {
						teamEmail = teamEmail + ", " + 
								team.getString("email");
					}
					teamEmail = teamEmail.replaceFirst(", ", new String());
					message = message.replace(
							"[TEAM]", 
							teamEmail
							);
					
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return message;
	}
}
